# MapBox Proc

## Overview 
This application works with hundred thousands of data represented with MapBox. 
There are "airports" (random generated dataset) on the map and we can draw or import polygons and represent the nearest "hotels" (random generated dataset too) to the airports inside that polygon.