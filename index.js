mapboxgl.accessToken = 'pk.eyJ1IjoiZGFnbzIxIiwiYSI6ImNqN2hjaWp0MjFnc2cyd28ycmVpY2Y4Z28ifQ.9KPkINCK4_JlwhbjRjncjQ';

var map, draw, airports, hotels, dropped, loadedPolygons = new Map();


$.getJSON('randAirports.json', function(response){
    airports = response;
    console.log('Airports loaded');
    addMapLoadEvent();

    dropped = false;
    $("#flip").click(function(){
        if(dropped){
            $("#panel").slideUp("slow");
        }else{
            $("#panel").slideDown("slow");
        }
        dropped = !dropped;
    });
});
$.getJSON('randHotels.json', function(response){
    hotels = response;
    console.log('Hotels loaded');
    addCalcButtonEvenet();
    addPolyBtnEvent()
});

function addPolyBtnEvent(){
    document.getElementById('importPolyBtn').onclick = function() {
        var polyUrl = $("#polyUrl").val();
        if(polyUrl){
            loadPolygonFromURL(polyUrl);
        }
    };
}

function addCalcButtonEvenet(){
    initDraw();
    document.getElementById('calculate').onclick = function() {
        var data = draw.getAll();
        data.features =  data.features.concat(Array.from( loadedPolygons.values() ));
        if (data.features.length > 0) {
            clearOldLayers();

            var airportsWithin = turf.within(airports, data);
            var nearestHotels = airportsWithin.features.map(function (x){return turf.nearest(x, hotels);})
            var source = {"type":"FeatureCollection","features": nearestHotels};

            addAirportToHotelRoutes(airportsWithin, nearestHotels);
            addHotelPoints(source);
        } else {
            alert("Use the draw tools to draw a polygon!");
        }
    };
}

function addMapLoadEvent(){
    initDraw();
    map.on('load', function() {

        map.addSource("airports", {
            type: "geojson",
            data: airports,
            cluster: true,
            clusterMaxZoom: 15,
            clusterRadius: 20
        });

        var stopsColor = [
            [0, 'rgb(242,240,247)'],
            [5, 'rgb(203,201,226)'],
            [20, 'rgb(158,154,200)'],
            [100, 'rgb(117,107,177)'],
            [200, 'rgb(84,39,143)']
        ];

        var stopsRadius = [
            [0, 10],
            [5, 11],
            [20, 12],
            [100, 13],
            [200, 15]
        ];

        map.addLayer({
            id: "clusters",
            type: "circle",
            source: "airports",
            filter: ["has", "point_count"],
            paint: {
                "circle-color": {
                    property: "point_count",
                    type: "interval",
                    stops: stopsColor
                },
                "circle-radius": {
                    property: "point_count",
                    type: "interval",
                    stops: stopsRadius
                }
            }
        });

        map.addLayer({
            id: "cluster-count",
            type: "symbol",
            source: "airports",
            filter: ["has", "point_count"],
            layout: {
                "text-field": "{point_count_abbreviated}",
                "text-font": ["DIN Offc Pro Medium", "Arial Unicode MS Bold"],
                "text-size": 12
            }
        });

        map.addLayer({
            id: "unclustered-point",
            type: "circle",
            source: "airports",
            filter: ["!has", "point_count"],
            paint: {
                "circle-color": "#11b4da",
                "circle-radius": 6,
                "circle-stroke-width": 1,
                "circle-stroke-color": "#fff"
            }
        });
    });
}

function initDraw(){
    if(!draw){
        map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v9',
            center: [-81.49658203125, 28.97931203672246],
            zoom: 6
        });

        draw = new MapboxDraw({
            displayControlsDefault: false,
            controls: {
                polygon: true,
                trash: true
            }
        });

        map.addControl(draw);
    }else{
       $("#downloadingDiv").hide();
       $("#calculationBoxDiv").show();
    }
}

function addHotelPoints(source){
    map.addSource("hotels", {
        type: "geojson",
        data: source,
        cluster: true,
        clusterMaxZoom: 15,
        clusterRadius: 20
    });

    map.addLayer({
        id: "hotels",
        type: "circle",
        source: "hotels",
        paint: {
            "circle-color": "red",
            "circle-radius": 5,
            "circle-stroke-width": 1,
            "circle-stroke-color": "#fff"
        }
    });
}

function addAirportToHotelRoutes(airportsWithin, nearestHotels){
    for (var i = 0; i < airportsWithin.features.length; ++i){
        map.addSource("airportToHotelRoute" + i, {
            "type": "geojson",
            "data": {
                "type": "Feature",
                "properties": {},
                "geometry": {
                    "type": "LineString",
                    "coordinates": [
                        airportsWithin.features[i].geometry.coordinates,
                        nearestHotels[i].geometry.coordinates,
                    ]
                }
            }
        });

        map.addLayer({
            "id": "airportToHotelRoute" + i,
            "type": "line",
            "source": "airportToHotelRoute" + i,
            "layout": {
                "line-join": "round",
                "line-cap": "round"
            },
            "paint": {
                "line-color": "#888",
                "line-width": 3
            }
        });
    }
}

function clearOldLayers() {
    if(map.getLayer('hotels')){
        map.removeLayer("hotels");
        map.removeSource("hotels");
    }
    var i = 0;
    while(map.getLayer("airportToHotelRoute" + i)){
        map.removeLayer("airportToHotelRoute" + i);
        map.removeSource("airportToHotelRoute" + i);
        ++i;
    }
}

function loadPolygonFromURL(url){
    if(url.toLowerCase().endsWith('.json') || url.toLowerCase().endsWith('.geojson') || url.toLowerCase().endsWith('.kml')){
        $.get( url, function(data) {
            var geoJson;
            try{
                geoJson = toGeoJSON.kml(data)
            }catch(ex){
                geoJson = data;
            }
            try{
                var isTherePolygon = false;
                for(var i = 0;i< geoJson.features.length; ++i){
                    if(geoJson.features[i].geometry.type.toLowerCase() === "polygon"){
                        isTherePolygon = true;
                        if(!loadedPolygons.has(url + "_" + i)){
                            loadedPolygons.set(url + "_" + i, geoJson.features[i]);
                            map.addLayer({
                                'id': url + "_" + i,
                                'type': 'fill',
                                'source': {
                                    'type': 'geojson',
                                    'data': geoJson.features[i]
                                },
                                'layout': {},
                                'paint': {
                                    'fill-color': '#088',
                                    'fill-opacity': 0.4
                                }
                            });
                        }
                        map.flyTo({
                            center: [
                                geoJson.features[i].geometry.coordinates[0][2][0],
                                geoJson.features[i].geometry.coordinates[0][2][1]
                            ]
                            , zoom:8
                        });
                    }
                }
                if(!isTherePolygon){
                    alert( "There was no polygon in the given source: " + url );
                }
            }catch(ex){
                alert( "Could not create polygon from url (" + url + "), error: " + ex.message);
            }
        }).fail(function() {
            alert( "Could not load data from " + url );
        });
    }else{
        alert( "Url must point to a json/geojson/kml file!" );
    }
}